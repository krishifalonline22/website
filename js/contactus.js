
$(document).ready(function ($) {
    $('#contact-form').submit(function () {

        let validation = true;
        var uemail = document.getElementById('email').value;
        var reg = /(([a-zA-Z0-9\-?\.?]+)@(([a-zA-Z0-9\-_]+\.)+)([a-z]{2,4}))+$/;
        if (!reg.test(uemail)) {
            alert("invalid email");
            this.email.focus();
            validation = false;
        }

        var mobile = document.getElementById('phone').value;
        var no = /^\d{10}$/;
        if (!no.test(mobile)) {
            alert("invalid Phone number");
            validation = false;
        }

        data = {
            name: $("#name").val(),
            email: $("#email").val(),
            phone: $("#phone").val(),
            msg: $("#msg").val()
        }
        if (validation) {

            $.ajax({
                type: "POST",
                beforeSend: function (xhr) {
                    xhr.setRequestHeader('Authorization', "Basic S3JJc0hpRmFMb045OktyaXNoaWZhbClAI3B3ZCgjQE9ubGluZQ==");
                },
                url: 'https://api.krishifal.com:5999/api/v2/admin/contact-us',
                dataType: 'json',
                data: {
                    name: $("#name").val(),
                    email: $("#email").val(),
                    phone: $("#phone").val(),
                    msg: $("#msg").val()
                },
                success: function (res) {
                    if (res.status) {
                        alert('Your contact details has been Submited! we will connect soon!')
                        location.reload();
                    } else {
                        alert('internal server err,please try after some time')
                    }
                },
                error: function (err) {
                    alert(err.statusText);
                }
            });
        }
        return false;
    });
});

